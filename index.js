// Import thư viện Express Js
const express = require("express");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;
// hoặc dùng cách khai báo
const methodMiddleware = (req, res, next) => {
    console.log(req.method)

    next();
};
// Middleware in ra thời gian mỗi lần chạy
app.use((req, res, next) => {
    console.log(new Date());

    next();
}, (req, res, next) => { // hoặc viết gộp vào nhau thành 1 chuỗi các middleware
    console.log(req.method)

    next();
},
/*methodMiddleware*/ // và gọi function trong middleware
);
/*
// Middleware in ra request method mỗi lần chạy
app.use((req, res, next) => {
    console.log(req.method)

    next();
});
*/
app.post("/", (req, res) => {
    console.log("Post method");
    res.status(200).json({
        message: "Post method"
    })
});

// Khai báo GET API trả ra simple message
app.get("/", (req, res) => {
    let today = new Date();
    let str = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    console.log(str);
    res.status(200).json({
        message: str
    })
});

// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
})